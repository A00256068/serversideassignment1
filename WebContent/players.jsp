<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
      <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Players</title>
</head>

<h2>Welcome : 
	<c:if test="${sessionScope.loggedinUser != null}">
<c:out value="${sessionScope.loggedinUser}"/>
</c:if> 
	</h2>
	
<h3>Please enter your football player of the year nomination</h3>

<body>
<form method="post" action="registerPlayer">
	Enter Player Name: <input type="text" name="name">
	</br>
	Enter Player Club: <input type="text" name="club">
	</br>
	Enter Player Nationality: <input type="text" name="nationality">
	</br>
	<input type="submit" value="Vote">
	</form>
</body>
</html>