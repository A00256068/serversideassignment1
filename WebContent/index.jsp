
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Index</title>
</head>
<body>
<h1>Welcome!</h1>
	

	<h2>Current User : 
	<c:if test="${sessionScope.loggedinUser != null}">
<c:out value="${sessionScope.loggedinUser}"/>
</c:if>
	</h2>

	<a href="register.jsp">
    <button>Register</button>
	</a>
	</br>
	<a href="login.jsp">
    <button>Log in</button>
	</a>
	</br>
		<a href="logout.jsp">
    <button>Log Out</button>
	</a>
	</br>
	<a href="deleteAccount.jsp">
    <button>Delete Account</button>
	</a>
	</br>

</body>
</html>