package model;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


public enum UserDAO {
	
	instance;
	
	
	public Connection getConnection() {
		
		Connection connection = null;
		
		try	
		{
			Class.forName("org.hsqldb.jdbcDriver");
			connection = DriverManager.getConnection("jdbc:hsqldb:hsql://localhost/oneDB","sa","");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	
		return connection;
	}
	
	

	public void save(User user) {
		
		Connection connection = getConnection();
		
		try {
			PreparedStatement psmt = connection.prepareStatement("INSERT INTO USER (name,password,address) VALUES(?,?,?)");
			
			psmt.setString(1, user.getUsername());
			psmt.setString(2, user.getPassword());
			psmt.setString(3, user.getAddress());
			
			psmt.executeUpdate();
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void savePlayer(Player player) {
		
		Connection connection = getConnection();
		
		try {
			PreparedStatement psmt = connection.prepareStatement("INSERT INTO Player (name,club,nationality) VALUES(?,?,?)");
			
			psmt.setString(1, player.getName());
			psmt.setString(2, player.getClub());
			psmt.setString(3, player.getNationality());
			
			psmt.executeUpdate();
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
		
		
		public void deleteUser(User user) {
			
			Connection connection = getConnection();
			
			try {
				PreparedStatement psmt = connection.prepareStatement("DELETE FROM USER where name = ?");
				
				psmt.setString(1, user.getUsername());
				//psmt.setString(2, user.getPassword());
				//psmt.setString(3, user.getAddress());
				
				psmt.executeUpdate();
				
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
public User checkLogin(User user) {
			
			Connection connection = getConnection();
			User user1 = null;
			try {
				PreparedStatement psmt = connection.prepareStatement("Select * from USER where name = ? and password = ?");
				
				psmt.setString(1, user.getUsername());
				psmt.setString(2, user.getPassword());
				//psmt.setString(3, user.getAddress());
				
				ResultSet rs = psmt.executeQuery();
				if(rs.next())
				{
					user1 = new User(rs.getString(1),rs.getString(2),rs.getString(3));
				}
				
				
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return user1;
		}
		
		
}

