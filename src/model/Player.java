package model;

import java.util.List;

public class Player {
	
	private String name;
	private String club;
	private String nationality;
	//private List<Player> players;
	
	public Player(String name,String club, String nationality)
	{
		super();
		this.setName(name);
		this.setClub(club);
		this.setNationality(nationality);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getClub() {
		return club;
	}

	public void setClub(String club) {
		this.club = club;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	
	public String toString() 
    { 
        return "| Name: "+ name + "| Club: " + club + "| Nationality: " + nationality + "|\n"; 
    }


	//public List<Player> getPlayer() {
	//	return players;
	//}


	//public void setPlayer(List<Player> players) {
	//	this.players= players;
	//}
	

}
